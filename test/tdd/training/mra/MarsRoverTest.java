package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test (expected = MarsRoverException.class)
	public void testException() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(-7,8);
		
		assertTrue(obstacle);
	}
	
	@Test
	public void testObstacleAndInitialization() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(7,8);
		
		assertTrue(obstacle);
	}
	
	@Test
	public void testRoverLandingStatus() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,N)", rover.executeCommand(""));
	}
	
	@Test
	public void testRotationRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,E)", rover.executeCommand("r"));
	}
	
	@Test
	public void testRotationLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,0,W)", rover.executeCommand("l"));
	}
	
	@Test
	public void testMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(0,1,N)", rover.executeCommand("f"));
	}
	
	@Test
	public void testMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		rover.setRoverStartingPosition("(5,8,E)");
		
		assertEquals("(4,8,E)", rover.executeCommand("b"));
	}
	
	@Test
	public void testMultipleMoves() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		assertEquals("(2,2,E)", rover.executeCommand("ffrff"));
	}

	@Test
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(7,8)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,9,N)", rover.executeCommand("b"));
	}
	
	@Test
	public void testObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand("ffrfff"));
	}
	
	@Test
	public void testMultipleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	public void testObstacleAndWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
	}
	
	@Test
	public void testObstacleWrappingAndTurnAroundThePlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}
}


